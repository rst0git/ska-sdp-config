"""High-level API tests on processing blocks."""

import os

import pytest

from ska_sdp_config import ConfigCollision, config, entity

# pylint: disable=missing-docstring,redefined-outer-name

PREFIX = "/__test_pb"

SCRIPT = {"kind": "realtime", "name": "test_rt_script", "version": "0.0.1"}

TEST_PROCESSING_BLOCK_ID = "test-pb-00000000-0000"
TEST_PROCESSING_BLOCK_STATE_FINISHED = {
    "resources_available": True,
    "state": "FINISHED",
    "receive_addresses": {"1": {"1": ["0.0.0.0", 1024]}},
}


# pylint: disable=duplicate-code
@pytest.fixture(scope="session")
def cfg():
    host = os.getenv("SDP_TEST_HOST", "127.0.0.1")
    with config.Config(global_prefix=PREFIX, host=host) as cfg:
        cfg.backend.delete(PREFIX, must_exist=False, recursive=True)
        yield cfg
        cfg.backend.delete(PREFIX, must_exist=False, recursive=True)


def test_simple_pb():

    for missing in ["kind", "name", "version"]:
        with pytest.raises(ValueError, match="Processing script must"):
            script = dict(SCRIPT)
            del script[missing]
            entity.ProcessingBlock("foo-bar", None, script)
    with pytest.raises(ValueError, match="Processing block ID"):
        entity.ProcessingBlock("asd_htb", None, SCRIPT)
    with pytest.raises(ValueError, match="Processing block ID"):
        entity.ProcessingBlock("foo/bar", None, SCRIPT)

    pblock = entity.ProcessingBlock("foo-bar", None, SCRIPT)
    # pylint: disable=W0123
    assert pblock == eval("entity." + repr(pblock))


def test_create_pblock(cfg):

    # Create 3 processing blocks
    for txn in cfg.txn():

        pblock1_id = txn.new_processing_block_id("test")
        pblock1 = entity.ProcessingBlock(pblock1_id, None, SCRIPT)
        assert txn.get_processing_block(pblock1_id) is None
        txn.create_processing_block(pblock1)
        with pytest.raises(ConfigCollision):
            txn.create_processing_block(pblock1)
        assert txn.get_processing_block(pblock1_id).pb_id == pblock1_id

        pblock2_id = txn.new_processing_block_id("test")
        pblock2 = entity.ProcessingBlock(pblock2_id, None, SCRIPT)
        txn.create_processing_block(pblock2)

        pblock_ids = txn.list_processing_blocks()
        assert pblock_ids == [pblock1_id, pblock2_id]

    # Make sure that it stuck
    for txn in cfg.txn():
        pblock_ids = txn.list_processing_blocks()
        assert pblock_ids == [pblock1_id, pblock2_id]

    # Make sure we can update them
    for txn in cfg.txn():
        pblock1.parameters["test"] = "test"
        pblock1.dependencies.append({"pblockId": pblock2_id, "kind": []})
        txn.update_processing_block(pblock1)

    # Check that update worked
    for txn in cfg.txn():
        pblock1x = txn.get_processing_block(pblock1.pb_id)
        assert pblock1x.eb_id is None
        assert pblock1x.parameters == pblock1.parameters
        assert pblock1x.dependencies == pblock1.dependencies


def test_take_pblock(cfg):

    script2 = dict(SCRIPT)
    script2["name"] += "-take"

    # Create another processing block
    for txn in cfg.txn():

        pblock_id = txn.new_processing_block_id("test")
        pblock = entity.ProcessingBlock(pblock_id, None, script2)
        txn.create_processing_block(pblock)

    with cfg.lease() as lease:

        for txn in cfg.txn():
            txn.take_processing_block(pblock_id, lease)

        for txn in cfg.txn():
            assert txn.get_processing_block_owner(pblock_id) == cfg.owner
            assert txn.is_processing_block_owner(pblock_id)

    for txn in cfg.txn():
        assert txn.get_processing_block_owner(pblock_id) is None
        assert not txn.is_processing_block_owner(pblock_id)


def test_pblock_state(cfg):
    pblock_id = f"state-test-{TEST_PROCESSING_BLOCK_ID}"
    state1 = {
        "resources_available": True,
        "state": "RUNNING",
        "receive_addresses": {"1": {"1": ["0.0.0.0", 1024]}},
    }
    state2 = TEST_PROCESSING_BLOCK_STATE_FINISHED

    # Create processing block
    for txn in cfg.txn():
        pblock = entity.ProcessingBlock(pblock_id, None, SCRIPT)
        txn.create_processing_block(pblock)

    # Check PBLOCK state is None
    for txn in cfg.txn():
        state_out = txn.get_processing_block_state(pblock_id)
        assert state_out is None

    # Create PBLOCK state as state1
    for txn in cfg.txn():
        txn.create_processing_block_state(pblock_id, state1)

    # Read PBLOCK state and check it matches state1
    for txn in cfg.txn():
        state_out = txn.get_processing_block_state(pblock_id)
        assert state_out == state1

    # Try to create PBLOCK state again and check it raises a collision
    # exception
    for txn in cfg.txn():
        with pytest.raises(ConfigCollision):
            txn.create_processing_block_state(pblock_id, state1)

    # Update PBLOCK state to state2
    for txn in cfg.txn():
        txn.update_processing_block_state(pblock_id, state2)

    # Read PBLOCK state and check it now matches state2
    for txn in cfg.txn():
        state_out = txn.get_processing_block_state(pblock_id)
        assert state_out == state2


def test_delete_processing_block_no_recurse(cfg):
    pblock_id = f"delete-test-no-rec-{TEST_PROCESSING_BLOCK_ID}"

    # Create a Processing block
    for txn in cfg.txn():
        pblock = entity.ProcessingBlock(pblock_id, None, SCRIPT)
        txn.create_processing_block(pblock)

    # Create a Processing block state
    for txn in cfg.txn():
        txn.create_processing_block_state(
            pblock_id, TEST_PROCESSING_BLOCK_STATE_FINISHED
        )

    # check that PB and its now exist
    for txn in cfg.txn():
        pblocks = txn.list_processing_blocks()
        pb_state = txn.get_processing_block_state(pblock_id)
        assert pblock_id in pblocks
        assert pb_state == TEST_PROCESSING_BLOCK_STATE_FINISHED

    # delete PB --> tested method
    for txn in cfg.txn():
        txn.delete_processing_block(pblock_id, recurse=False)

    # check that pb is deleted, but its state isn't
    for txn in cfg.txn():
        pblocks = txn.list_processing_blocks()
        pb_state = txn.get_processing_block_state(pblock_id)
        assert pblock_id not in pblocks
        assert pb_state == TEST_PROCESSING_BLOCK_STATE_FINISHED


def test_delete_processing_block_with_recurse(cfg):
    pblock_id = f"delete-test-rec-{TEST_PROCESSING_BLOCK_ID}"

    # Create a Processing block (PB)
    for txn in cfg.txn():
        pblock = entity.ProcessingBlock(pblock_id, None, SCRIPT)
        txn.create_processing_block(pblock)

    # Create a Processing block state
    for txn in cfg.txn():
        txn.create_processing_block_state(
            pblock_id, TEST_PROCESSING_BLOCK_STATE_FINISHED
        )

    # check that PB and its state now exist
    for txn in cfg.txn():
        pblocks = txn.list_processing_blocks()
        pb_state = txn.get_processing_block_state(pblock_id)
        assert pblock_id in pblocks
        assert pb_state == TEST_PROCESSING_BLOCK_STATE_FINISHED

    # delete PB and its state -- tested method
    for txn in cfg.txn():
        txn.delete_processing_block(pblock_id, recurse=True)

    # check that both PB and its state are deleted
    for txn in cfg.txn():
        pblocks = txn.list_processing_blocks()
        pb_state = txn.get_processing_block_state(pblock_id)
        assert pblock_id not in pblocks
        assert pb_state is None


if __name__ == "__main__":
    pytest.main()
