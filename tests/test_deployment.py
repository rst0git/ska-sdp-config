"""High-level API tests on Deployment entries."""

import os

import pytest

import ska_sdp_config

# pylint: disable=missing-docstring,redefined-outer-name

PREFIX = "/__test_deploy"


# pylint: disable=W0212
@pytest.fixture(scope="session")
def cfg():
    host = os.getenv("SDP_TEST_HOST", "127.0.0.1")
    with ska_sdp_config.Config(global_prefix=PREFIX, host=host) as cfg:
        cfg._backend.delete(PREFIX, must_exist=False, recursive=True)
        yield cfg
        cfg._backend.delete(PREFIX, must_exist=False, recursive=True)


def test_deploy_create(cfg):

    deploy_id = "proc-20210824-00099-script"
    chart = {"chart": "script", "values": {}}

    deploy = ska_sdp_config.Deployment(deploy_id, "helm", chart)

    # Deployment has not been created, so should return None
    for txn in cfg.txn():
        state = txn.get_deployment(deploy_id)
        assert state is None

    # Create Deployment
    for txn in cfg.txn():
        txn.create_deployment(deploy)

    # Read Deployment and check it is equal to input value
    for txn in cfg.txn():
        state = txn.get_deployment(deploy_id)
        assert state == deploy

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.create_deployment(deploy)


def test_deploy_state(cfg):

    deploy_id = "proc-20210824-00099-script"

    # Deployment state has not been created, so should return None
    for txn in cfg.txn():
        state = txn.get_deployment_state(deploy_id)
        assert state is None

    # Create Deployment state
    for txn in cfg.txn():
        txn.create_deployment_state(deploy_id, {"state": "RUNNING"})

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.create_deployment_state(deploy_id, {"state": "RUNNING"})

    # Read Deployment state and check it is equal to input value
    for txn in cfg.txn():
        state = txn.get_deployment_state(deploy_id)
        assert state == {"state": "RUNNING"}

    # Update Deployment state
    for txn in cfg.txn():
        txn.update_deployment_state(deploy_id, {"state": "FINISHED"})

    # Read Deployment state and check it is equal to updated value
    for txn in cfg.txn():
        state = txn.get_deployment_state(deploy_id)
        assert state == {"state": "FINISHED"}


def test_deploy_delete(cfg):
    deploy_id = "test-delete-deploy-000000-0000"
    chart = {"chart": "script", "values": {}}

    deploy = ska_sdp_config.Deployment(deploy_id, "helm", chart)

    # Create Deployment
    for txn in cfg.txn():
        txn.create_deployment(deploy)
        txn.create_deployment_state(deploy_id, {"state": "FINISHED"})

    # check that deployment and its state now exist
    for txn in cfg.txn():
        deployments = txn.list_deployments()
        deploy_state = txn.get_deployment_state(deploy_id)
        assert deploy_id in deployments
        assert deploy_state == {"state": "FINISHED"}

    # delete PB and its state -- tested method
    for txn in cfg.txn():
        txn.delete_deployment(deploy)

    # check that both deployment and its state are deleted
    for txn in cfg.txn():
        deployments = txn.list_deployments()
        deploy_state = txn.get_deployment_state(deploy_id)
        assert deploy_id not in deployments
        assert deploy_state is None
