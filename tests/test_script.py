"""High-level API tests on script."""

import os

import pytest

import ska_sdp_config

# pylint: disable=missing-docstring,redefined-outer-name

PREFIX = "/__test_script"
SCRIPT_IMAGE = "nexus/script-test"


# pylint: disable=W0212
@pytest.fixture(scope="session")
def cfg():
    host = os.getenv("SDP_TEST_HOST", "127.0.0.1")
    with ska_sdp_config.Config(global_prefix=PREFIX, host=host) as cfg:
        cfg._backend.delete(PREFIX, must_exist=False, recursive=True)
        yield cfg
        cfg._backend.delete(PREFIX, must_exist=False, recursive=True)


def test_script_list_all(cfg):
    """Test list all scripts."""

    script_kind = "realtime"
    script_name = "test_realtime"
    script_version = "0.0.1"

    script = {"image": f"{SCRIPT_IMAGE}:{script_version}"}

    # Check the script list is empty
    for txn in cfg.txn():
        scripts = txn.list_scripts()
        assert scripts == []

    # Create script
    for txn in cfg.txn():
        txn.create_script(script_kind, script_name, script_version, script)

    # List all the scripts
    for txn in cfg.txn():
        script_keys = txn.list_scripts()
        for kind, name, version in script_keys:
            assert kind == script_kind
            assert name == script_name
            assert version == script_version


def test_script_list_kind(cfg):
    """Test scripts with specific kind."""

    script_kind = "batch"
    script_name = "test_batch"
    script_version = "0.0.1"

    script = {"image": f"{SCRIPT_IMAGE}:{script_version}"}

    # Create script
    for txn in cfg.txn():
        txn.create_script(script_kind, script_name, script_version, script)

    # List scripts with specified kind
    for txn in cfg.txn():
        script_keys = txn.list_scripts(script_kind)
        for kind, name, version in script_keys:
            assert kind == script_kind
            assert name == script_name
            assert version == script_version


def test_script_list_kind_name(cfg):
    """Test scripts with specific kind and name."""

    script_kind = "batch"
    script_name = "test_vis_receive"
    script_version = "0.0.1"

    script = {"image": f"{SCRIPT_IMAGE}:{script_version}"}

    # Create script
    for txn in cfg.txn():
        txn.create_script(script_kind, script_name, script_version, script)

    # List scripts of specified kind and name
    for txn in cfg.txn():
        script_keys = txn.list_scripts(script_kind, script_name)
        for kind, name, version in script_keys:
            assert kind == script_kind
            assert name == script_name
            assert version == script_version


def test_script_create_update(cfg):
    """Test create and update script."""

    script_kind = "batch"
    script_name = "test_batch"
    script_version = "0.2.1"

    script = {"image": f"{SCRIPT_IMAGE}:{script_version}"}
    script2 = {
        "image": "nexus/script-test-realtime:0.2.1",
    }

    # Create script
    for txn in cfg.txn():
        txn.create_script(script_kind, script_name, script_version, script)

    # Read script and check it is equal to script
    for txn in cfg.txn():
        get = txn.get_script(script_kind, script_name, script_version)
        assert get == script

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.create_script(script_kind, script_name, script_version, script)

    # Update script to script2
    for txn in cfg.txn():
        txn.update_script(script_kind, script_name, script_version, script2)

    # Read script and check it is equal to script2
    for txn in cfg.txn():
        get = txn.get_script(script_kind, script_name, script_version)
        assert get == script2


def test_delete_script(cfg):
    """Test deleting script."""

    script_kind = "realtime"
    script_name = "test_cbf_recv"
    script_version = "0.1.0"

    script = {"image": f"{SCRIPT_IMAGE}:{script_version}"}

    # Create script
    for txn in cfg.txn():
        txn.create_script(script_kind, script_name, script_version, script)

    # Read script and check it is equal to script
    for txn in cfg.txn():
        get = txn.get_script(script_kind, script_name, script_version)
        assert get == script

    # Delete script
    for txn in cfg.txn():
        txn.delete_script(script_kind, script_name, script_version)

    # Read script and check it is equal to None
    for txn in cfg.txn():
        get = txn.get_script(script_kind, script_name, script_version)
        assert get is None


if __name__ == "__main__":
    pytest.main()
