"""High-level API tests on execution blocks."""

import os

import pytest

import ska_sdp_config

# pylint: disable=missing-docstring,redefined-outer-name

PREFIX = "/__test_eb"


# pylint: disable=W0212
@pytest.fixture(scope="session")
def cfg():
    host = os.getenv("SDP_TEST_HOST", "127.0.0.1")
    with ska_sdp_config.Config(global_prefix=PREFIX, host=host) as cfg:
        cfg._backend.delete(PREFIX, must_exist=False, recursive=True)
        yield cfg
        cfg._backend.delete(PREFIX, must_exist=False, recursive=True)


def test_eb_list(cfg):
    # Check execution block list is empty
    for txn in cfg.txn():
        eb_ids = txn.list_execution_blocks()
        assert eb_ids == []

    # Create first execution block
    for txn in cfg.txn():
        eb1_id = txn.new_execution_block_id("test")

    for txn in cfg.txn():
        txn.create_execution_block(eb1_id, {})

    # Check execution block list has entry
    for txn in cfg.txn():
        eb_ids = txn.list_execution_blocks()
        assert eb_ids == [eb1_id]

    # Create second execution block
    for txn in cfg.txn():
        eb2_id = txn.new_execution_block_id("test")

    for txn in cfg.txn():
        txn.create_execution_block(eb2_id, {})

    # Check execution block list has both entries
    for txn in cfg.txn():
        eb_ids = txn.list_execution_blocks()
        assert eb_ids == sorted([eb1_id, eb2_id])


def test_eb_create_update(cfg):

    for txn in cfg.txn():
        eb_id = txn.new_execution_block_id("test")

    state1 = {
        "scan_id": None,
        "pb_realtime": [],
        "pb_batch": [],
        "pb_receive_addresses": None,
    }
    state2 = {
        "scan_id": 1,
        "pb_realtime": ["realtime-20200213-0001", "realtime-20200213-0002"],
        "pb_batch": ["batch-20200213-0001", "batch-20200213-0002"],
        "pb_receive_addresses": "realtime-20200213-0001",
    }

    # Execution block has not been created, so should return None
    for txn in cfg.txn():
        state = txn.get_execution_block(eb_id)
        assert state is None

    # Create execution block as state1
    for txn in cfg.txn():
        txn.create_execution_block(eb_id, state1)

    # Read execution block and check it is equal to state1
    for txn in cfg.txn():
        state = txn.get_execution_block(eb_id)
        assert state == state1

    # Trying to recreate should raise a collision exception
    for txn in cfg.txn():
        with pytest.raises(ska_sdp_config.ConfigCollision):
            txn.create_execution_block(eb_id, state1)

    # Update execution block to state2
    for txn in cfg.txn():
        txn.update_execution_block(eb_id, state2)

    # Read execution block and check it is equal to state2
    for txn in cfg.txn():
        state = txn.get_execution_block(eb_id)
        assert state == state2


def test_eb_delete(cfg):
    eb_id = "test-delete-eb-00000-0000"
    eb_state = {
        "scan_id": None,
        "pb_realtime": [],
        "pb_batch": [],
        "pb_receive_addresses": None,
    }

    # Create a Execution block (EB)
    for txn in cfg.txn():
        txn.create_execution_block(eb_id, eb_state)

    # check that EB now exists
    for txn in cfg.txn():
        eblocks = txn.list_execution_blocks()
        assert eb_id in eblocks

    # delete EB and its state -- tested method
    for txn in cfg.txn():
        txn.delete_execution_block(eb_id, recurse=True)

    # check that EB is deleted
    for txn in cfg.txn():
        eblocks = txn.list_processing_blocks()
        assert eb_id not in eblocks


if __name__ == "__main__":
    pytest.main()
