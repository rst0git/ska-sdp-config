
# Configuration Schema

This is the schema of the configuration database, effectively the control plane
of the SDP.

## Execution Block

Path `/eb/[eb_id]`

Dynamic state information of the execution block.

Contents:
```javascript
{
    "eb_id": "eb-mvp01-20200425-00000",
    "max_length": 21600.0,
    "scan_types": [
        { "scan_type_id": "science", ... },
        { "scan_type_id": "calibration", ... }
    ],
    "pb_realtime": [ "pb-mvp01-20200425-00000", ... ],
    "pb_batch": [ ... ],
    "pb_receive_addresses": "pb-mvp01-20200425-00000",
    "current_scan_type": "science",
    "status": "SCANNING",
    "scan_id": 12345,
    "last_updated": "2022-08-01 10:01:12"
}
```

When the execution block is being executed, the `status` field is set to the
observation state (`obsState`) of the subarray. When the execution block is
ended, `status` is set to `FINISHED`.


## Processing Block

Path: `/pb/[pb_id]`

Static definition of processing block information.

Contents:
```javascript
{
    "pb_id": "pb-mvp01-20200425-00000",
    "eb_id": "eb-mvp01-20200425-00000",
    "script": {
        "kind": "realtime",
        "name": "vis_receive",
        "version": "0.2.0"
    },
    "parameters": { ... }
}
```

There are two kinds of processing, real-time and batch (offline). Real-time
processing starts immediately, as it directly corresponds to an observation
that is about to start. Batch processing will be inserted into a scheduling
queue managed by the SDP, where it will typically be executed according to
resource availability.

Valid kinds are `realtime` and `batch`. The script tag identifies the
processing script version as well as the required underlying software (e.g.
execution engines, processing components). `...` stands for arbitrary
processing script-defined parameters.

### Processing Block State

Path: `/pb/[pb_id]/state`

Dynamic state information of the processing block. If it does not exist, the
processing block is still starting up.

Contents:
```javascript
{
    "resources_available": True,
    "status": "RUNNING",
    "receive_addresses": [
        { "scan_type_id": "science", ... },
        { "scan_type_id": "calibration", ... },
    ],
    "last_updated": "2022-08-01 10:01:12"
}
```

Tracks the current state of the processing block. This covers both the
SDP-internal state (as defined by the Execution Control Data Model) as well as
information to publish via Tango for real-time processing, such as the status
and receive addresses (for ingest).

### Processing Block Owner

Path: `/pb/[pb_id]/owner`

Identifies the process executing the script. Used for leader election/lock as
well as a debugging aid.

Contents:
```javascript
{
  "command": [
    "vis_receive.py",
    "pb-mvp01-20200425-00000"
  ],
  "hostname": "pb-mvp01-20200425-00000-script-2kxfz",
  "pid": 1
}
```
