# Changelog

## 0.3.4

* Add methods for getting, creating and updating deployment state.

## 0.3.3

* Update CLI to create realtime PBs and end SBIs.

## 0.3.2

* Allow CLI to update and edit LMC device entries.
* Improve behaviour of CLI on Windows.

## 0.3.1

* Bugfix in `ska-sdp` CLI: use correct Config DB prefix for SBIs: `sb` instead of `sbi`.
* Publish package in new central artefact repository.

## 0.3.0

* New CLI, called `ska-sdp` added and `sdpcfg` removed.
* Config.txn() can now create/delete/list workflow definitions from the Config DB.
