include .make/base.mk
include .make/python.mk

PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503
