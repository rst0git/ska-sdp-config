#/bin/sh

# Run a python container for testing code. It connects to the etcd instance
# created by docker_run_etcd.sh.

version=3.10

echo
echo Starting python container. The local directory will be mounted in the
echo container.
echo
echo First install the dependencies:
echo  "$ curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python -"
echo  $ /opt/poetry/bin/poetry config virtualenvs.create false
echo  $ /opt/poetry/bin/poetry install
echo
echo Then run the tests:
echo  $ make python-test
echo
echo If you get an error message about the etcd container below, you need to
echo start it first with docker_run_etcd.sh.
echo

docker run -it -v $(PWD):/app -w /app --rm --link etcd \
    --env SDP_CONFIG_HOST=etcd --env SDP_TEST_HOST=etcd \
    python:${version} bash
