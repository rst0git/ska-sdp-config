"""High-level API for SKA SDP configuration."""

import json
import os
import sys
from datetime import date
from socket import gethostname
from typing import Iterable

from . import backend as backend_mod
from . import entity


class Config:
    """Connection to SKA SDP configuration."""

    def __init__(self, backend=None, global_prefix="", owner=None, **cargs):
        """
        Connect to configuration using the given backend.

        :param backend: Backend to use. Defaults to environment or etcd3 if
            not set.
        :param global_prefix: Prefix to use within the database
        :param owner: Dictionary used for identifying the process when claiming
            ownership.
        :param cargs: Backend client arguments
        """
        self._backend = self._determine_backend(backend, **cargs)

        # Owner dictionary
        if owner is None:
            owner = {
                "pid": os.getpid(),
                "hostname": gethostname(),
                "command": sys.argv,
            }
        self.owner = dict(owner)

        # Prefixes
        assert global_prefix == "" or global_prefix[0] == "/"
        self._paths = {
            "pb": global_prefix + "/pb/",
            "eb": global_prefix + "/eb/",
            "controller": global_prefix + "/lmc/controller",
            "subarray": global_prefix + "/lmc/subarray/",
            "deploy": global_prefix + "/deploy/",
            "script": global_prefix + "/script/",
        }

        # Lease associated with client
        self._client_lease = None

    @property
    def backend(self):
        """Get the backend database object."""
        return self._backend

    @staticmethod
    def _determine_backend(backend, **cargs):

        # Determine backend
        if not backend:
            backend = os.getenv("SDP_CONFIG_BACKEND", "etcd3")

        # Instantiate backend, reading configuration from environment/dotenv
        if backend == "etcd3":

            if "host" not in cargs:
                cargs["host"] = os.getenv("SDP_CONFIG_HOST", "127.0.0.1")
            if "port" not in cargs:
                cargs["port"] = int(os.getenv("SDP_CONFIG_PORT", "2379"))
            if "protocol" not in cargs:
                cargs["protocol"] = os.getenv("SDP_CONFIG_PROTOCOL", "http")
            if "cert" not in cargs:
                cargs["cert"] = os.getenv("SDP_CONFIG_CERT", None)
            if "username" not in cargs:
                cargs["username"] = os.getenv("SDP_CONFIG_USERNAME", None)
            if "password" not in cargs:
                cargs["password"] = os.getenv("SDP_CONFIG_PASSWORD", None)

            return backend_mod.Etcd3Backend(**cargs)

        if backend == "memory":

            return backend_mod.MemoryBackend()

        raise ValueError(f"Unknown configuration backend {backend}!")

    def lease(self, ttl=10):
        """
        Generate a new lease.

        Once entered can be associated with keys,
        which will be kept alive until the end of the lease. At that
        point a daemon thread will be started automatically to refresh
        the lease periodically (default seems to be TTL/4).

        :param ttl: Time to live for lease
        :returns: lease object
        """
        return self._backend.lease(ttl)

    @property
    def client_lease(self):
        """Return the lease associated with the client.

        It will be kept alive until the client gets closed.
        """
        # pylint: disable=unnecessary-dunder-call
        if self._client_lease is None:
            self._client_lease = self.lease()
            self._client_lease.__enter__()

        return self._client_lease

    def txn(self, max_retries: int = 64) -> Iterable["Transaction"]:
        """Create a :class:`Transaction` for atomic configuration query/change.

        As we do not use locks, transactions might have to be repeated in
        order to guarantee atomicity. Suggested usage is as follows:

        .. code-block:: python

            for txn in config.txn():
                # Use txn to read+write configuration
                # [Possibly call txn.loop()]

        As the `for` loop suggests, the code might get run multiple
        times even if not forced by calling
        :meth:`Transaction.loop`. Any writes using the transaction
        will be discarded if the transaction fails, but the
        application must make sure that the loop body has no other
        observable side effects.

        See also :ref:`Usage Guide <usage-guide>` for best practices
        for using transactions.

        :param max_retries: Number of transaction retries before a
            :class:`RuntimeError` gets raised.

        """

        for txn in self._backend.txn(max_retries=max_retries):
            yield Transaction(self, txn, self._paths)

    def watcher(self, timeout=None) -> Iterable:
        """Create a new watcher.

        Useful for waiting for changes in the configuration. Calling
        :py:meth:`Etcd3Watcher.txn()` on the returned watchers will
        create :py:class:`Transaction` objects just like
        :py:meth:`txn()`.

        See also :ref:`Usage Guide <usage-guide>` for best practices
        for using watchers.

        :param timeout: Timeout for waiting. Watcher will loop after this time.

        """

        def txn_wrapper(txn):
            return Transaction(self, txn, self._paths)

        for watcher in self._backend.watcher(timeout, txn_wrapper):
            yield watcher

    def close(self):
        """Close the client connection."""
        if self._client_lease:
            self._client_lease.__exit__(None, None, None)
            self._client_lease = None
        self._backend.close()

    def __enter__(self):
        """Scope the client connection."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Scope the client connection."""
        self.close()
        return False


def dict_to_json(obj):
    """Format a dictionary for writing it into the database.

    :param obj: Dictionary object to format
    :returns: String representation
    """
    # We only write dictionaries (JSON objects) at the moment
    assert isinstance(obj, dict)
    # Export to JSON. No need to convert to ASCII, as the backend
    # should handle unicode. Otherwise we optimise for legibility
    # over compactness.
    return json.dumps(
        obj,
        ensure_ascii=False,
        indent=2,
        separators=(",", ": "),
        sort_keys=True,
    )


class Transaction:
    """High-level configuration queries and updates to execute atomically."""

    # pylint: disable=too-many-public-methods

    def __init__(self, config, txn, paths):
        """Instantiate transaction."""
        self._cfg = config
        self._txn = txn
        self._paths = paths

    @property
    def raw(self):
        """Return transaction object for accessing database directly."""
        return self._txn

    def _get(self, path):
        """Get a JSON object from the database."""
        txt = self._txn.get(path)
        if txt is None:
            return None
        return json.loads(txt)

    def _create(self, path, obj, lease=None):
        """Set a new path in the database to a JSON object."""
        self._txn.create(path, dict_to_json(obj), lease)

    def _update(self, path, obj):
        """Set a existing path in the database to a JSON object."""
        self._txn.update(path, dict_to_json(obj))

    def _delete(self, path, recurse=True):
        """
        Delete one or more JSON objects from the database

        :param path: path to JSON object in DB
        :param recurse: if True, delete object recursively
                        (i.e. all objects whose paths start with path)
        """
        if recurse:
            for key in self._txn.list_keys(path, recurse=8):
                self._txn.delete(key)
        else:
            self._txn.delete(path)

    def loop(self, wait=False, timeout=None):
        """Repeat transaction regardless of whether commit succeeds.

        :param wait: If transaction succeeded, wait for any read
            values to change before repeating it.
        :param timeout: Maximum time to wait, in seconds
        """
        return self._txn.loop(wait, timeout)

    def list_processing_blocks(self, prefix=""):
        """Query processing block IDs from the configuration.

        :param prefix: If given, only search for processing block IDs
           with the given prefix
        :returns: Processing block ids, in lexicographical order
        """
        # List keys
        pb_path = self._paths["pb"]
        keys = self._txn.list_keys(pb_path + prefix)

        # return list, stripping the prefix
        assert all(key.startswith(pb_path) for key in keys)
        return list(key[len(pb_path) :] for key in keys)

    def new_processing_block_id(self, generator: str):
        """Generate a new processing block ID that is not yet in use.

        :param generator: Name of the generator
        :returns: Processing block ID
        """
        # Find existing processing blocks with same prefix
        pb_id_prefix = f"pb-{generator}-{date.today():%Y%m%d}"
        existing_ids = self.list_processing_blocks(pb_id_prefix)

        # Choose ID that doesn't exist
        for pb_ix in range(100000):
            pb_id = f"{pb_id_prefix}-{pb_ix:05}"
            if pb_id not in existing_ids:
                break
        if pb_ix >= 100000:
            raise RuntimeError("Exceeded daily number of processing blocks!")
        return pb_id

    def get_processing_block(self, pb_id: str) -> entity.ProcessingBlock:
        """
        Look up processing block data.

        :param pb_id: Processing block ID to look up
        :returns: Processing block entity, or None if it doesn't exist
        """
        dct = self._get(self._paths["pb"] + pb_id)
        if dct is None:
            return None
        return entity.ProcessingBlock(**dct)

    def create_processing_block(self, pblock: entity.ProcessingBlock):
        """
        Add a new :class:`ProcessingBlock` to the configuration.

        :param pb: Processing block to create
        """
        assert isinstance(pblock, entity.ProcessingBlock)
        self._create(self._paths["pb"] + pblock.pb_id, pblock.to_dict())

    def update_processing_block(self, pblock: entity.ProcessingBlock):
        """
        Update a :class:`ProcessingBlock` in the configuration.

        :param pb: Processing block to update
        """
        assert isinstance(pblock, entity.ProcessingBlock)
        self._update(self._paths["pb"] + pblock.pb_id, pblock.to_dict())

    def get_processing_block_owner(self, pb_id: str) -> dict:
        """
        Look up the current processing block owner.

        :param pb_id: Processing block ID to look up
        :returns: Processing block owner data, or None if not claimed
        """
        dct = self._get(self._paths["pb"] + pb_id + "/owner")
        if dct is None:
            return None
        return dct

    def is_processing_block_owner(self, pb_id: str) -> bool:
        """
        Check whether this client is owner of the processing block.

        :param pb_id: Processing block ID to look up
        :returns: Whether processing block exists and is claimed
        """
        return (
            self.get_processing_block(pb_id) is not None
            and self.get_processing_block_owner(pb_id) == self._cfg.owner
        )

    def take_processing_block(self, pb_id: str, lease):
        """
        Take ownership of the processing block.

        :param pb_id: Processing block ID to take ownership of
        :raises: backend.ConfigCollision
        """
        # Lease must be provided
        assert lease is not None

        # Provide information identifying this process
        self._create(
            self._paths["pb"] + pb_id + "/owner", self._cfg.owner, lease
        )

    def get_processing_block_state(self, pb_id: str) -> dict:
        """
        Get the current processing block state.

        :param pb_id: Processing block ID
        :returns: Processing block state, or None if not present
        """
        state = self._get(self._paths["pb"] + pb_id + "/state")
        if state is None:
            return None
        return state

    def create_processing_block_state(self, pb_id: str, state: dict):
        """
        Create processing block state.

        :param pb_id: Processing block ID
        :param state: Processing block state to create
        """
        self._create(self._paths["pb"] + pb_id + "/state", state)

    def update_processing_block_state(self, pb_id: str, state: dict):
        """
        Update processing block state.

        :param pb_id: Processing block ID
        :param state: Processing block state to update
        """
        self._update(self._paths["pb"] + pb_id + "/state", state)

    def delete_processing_block(self, pb_id: str, recurse: bool = True):
        """
        Delete a processing block (pb)

        :param pb_id: Processing block ID
        :param recurse: if True, run recursive query and delete all
                        includes deleting /state and /owner of pb if exists
        """
        path = self._paths["pb"] + pb_id
        self._delete(path, recurse=recurse)

    def get_deployment(self, deploy_id: str) -> entity.Deployment:
        """
        Retrieve details about a cluster configuration change.

        :param deploy_id: Name of the deployment
        :returns: Deployment details
        """
        dct = self._get(self._paths["deploy"] + deploy_id)
        if dct is None:
            return None
        return entity.Deployment(**dct)

    def list_deployments(self, prefix=""):
        """
        List all current deployments.

        :returns: Deployment IDs
        """
        # List keys
        keys = self._txn.list_keys(self._paths["deploy"] + prefix)

        # return list, stripping the prefix
        assert all(key.startswith(self._paths["deploy"]) for key in keys)
        return list(key[len(self._paths["deploy"]) :] for key in keys)

    def create_deployment(self, dpl: entity.Deployment):
        """
        Request a change to cluster configuration.

        :param dpl: Deployment to add to database
        """
        # Add to database
        assert isinstance(dpl, entity.Deployment)
        self._create(self._paths["deploy"] + dpl.dpl_id, dpl.to_dict())

    def delete_deployment(self, dpl: entity.Deployment):
        """
        Undo a change to cluster configuration.

        :param dpl: Deployment to remove
        """
        # Delete all data associated with deployment
        deploy_path = self._paths["deploy"] + dpl.dpl_id
        self._delete(deploy_path, recurse=True)

    def get_deployment_state(self, deploy_id: str) -> dict:
        """
        Get the current Deployment state.

        :param deploy_id: Deployment ID
        :returns: Deployment state, or None if not present
        """
        state = self._get(self._paths["deploy"] + deploy_id + "/state")
        if state is None:
            return None
        return state

    def create_deployment_state(self, deploy_id: str, state: dict):
        """
        Create Deployment state.

        :param deploy_id: Deployment ID
        :param state: Deployment state to create
        """
        self._create(self._paths["deploy"] + deploy_id + "/state", state)

    def update_deployment_state(self, deploy_id: str, state: dict):
        """
        Update Deployment state.

        :param deploy_id: Deployment ID
        :param state: Deployment state to update
        """
        self._update(self._paths["deploy"] + deploy_id + "/state", state)

    def list_execution_blocks(self, prefix=""):
        """Query execution block IDs from the configuration.

        :param prefix: if given, only search for execution block IDs
           with the given prefix
        :returns: execution block IDs, in lexicographical order
        """
        # List keys
        eb_path = self._paths["eb"]
        keys = self._txn.list_keys(eb_path + prefix)

        # Return list, stripping the prefix
        assert all(key.startswith(eb_path) for key in keys)
        return list(key[len(eb_path) :] for key in keys)

    def new_execution_block_id(self, generator: str):
        """Generate a new execution block ID that is not yet in use.

        :param generator: Name of the generator
        :returns: execution block ID
        """
        # Find existing execution blocks with same prefix
        eb_id_prefix = f"eb-{generator}-{date.today():%Y%m%d}"
        existing_ids = self.list_execution_blocks(eb_id_prefix)

        # Choose ID that doesn't exist
        for eb_ix in range(100000):
            eb_id = f"{eb_id_prefix}-{eb_ix:05}"
            if eb_id not in existing_ids:
                break
        if eb_ix >= 100000:
            raise RuntimeError("Exceeded daily number of execution blocks!")
        return eb_id

    def get_execution_block(self, eb_id: str) -> dict:
        """
        Get execution block.

        :param eb_id: execution block ID
        :returns: execution block state
        """
        state = self._get(self._paths["eb"] + eb_id)
        return state

    def create_execution_block(self, eb_id: str, state: dict):
        """
        Create execution block.

        :param eb_id: execution block ID
        :param state: execution block state
        """
        self._create(self._paths["eb"] + eb_id, state)

    def update_execution_block(self, eb_id: str, state: dict):
        """
        Update execution block.

        :param eb_id: execution block ID
        :param state: execution block state
        """
        self._update(self._paths["eb"] + eb_id, state)

    def delete_execution_block(self, eb_id: str, recurse: bool = True):
        """
        Delete an execution block (eb)

        :param eb_id: Execution block ID
        :param recurse: if True, run recursive query and delete all objects
        """
        path = self._paths["eb"] + eb_id
        self._delete(path, recurse=recurse)

    def list_subarrays(self, prefix=""):
        """Query subarray IDs from the configuration.

        :param prefix: if given, only search for subarray IDs
           with the given prefix
        :returns: subarray IDs, in lexicographical order
        """
        # List keys
        subarray_path = self._paths["subarray"]
        keys = self._txn.list_keys(subarray_path + prefix)

        # Return list, stripping the prefix
        assert all(key.startswith(subarray_path) for key in keys)
        return list(key[len(subarray_path) :] for key in keys)

    def get_subarray(self, subarray_id: str) -> dict:
        """
        Get subarray state.

        :param subarray_id: subarray ID
        :returns: subarray state
        """
        state = self._get(self._paths["subarray"] + subarray_id)
        return state

    def create_subarray(self, subarray_id: str, state: dict):
        """
        Create subarray state.

        :param subarray_id: subarray ID
        :param state: subarray state
        """
        self._create(self._paths["subarray"] + subarray_id, state)

    def update_subarray(self, subarray_id: str, state: dict):
        """
        Update subarray state.

        :param subarray_id: subarray ID
        :param state: subarray state
        """
        self._update(self._paths["subarray"] + subarray_id, state)

    def create_controller(self, state: dict) -> None:
        """
        Create controller state.

        :param state: controller state
        """
        self._create(self._paths["controller"], state)

    def update_controller(self, state: dict) -> None:
        """
        Update controller state.

        :param state: controller state
        """
        self._update(self._paths["controller"], state)

    def get_controller(self) -> dict:
        """
        Get controller state.

        :returns: controller state
        """
        state = self._get(self._paths["controller"])
        return state

    def create_script(
        self, kind: str, name: str, version: str, script: dict
    ) -> None:
        """
        Create processing script definition.

        :param kind: script kind
        :param name: script name
        :param version: script version
        :param script: script definition
        """
        self._create(self._script_path(kind, name, version), script)

    def get_script(self, kind: str, name: str, version: str) -> dict:
        """
        Get processing script definition.

        :param kind: script kind
        :param name: script name
        :param version: script version

        :returns: script definition
        """
        script = self._get(self._script_path(kind, name, version))
        return script

    def list_scripts(self, kind: str = "", name: str = "") -> list:
        """
        List processing script definitions.

        :param kind: script kind. Default empty
        :param name: script name. Default empty

        :returns: list of script definitions
        """

        # List all or specific keys
        script_path = self._paths["script"]
        if not kind:
            keys = self._txn.list_keys(script_path)
        elif not name:
            keys = self._txn.list_keys(script_path + kind)
        else:
            keys = self._txn.list_keys(script_path + kind + ":" + name)

        # Return list, stripping the prefix
        return list(tuple(key[len(script_path) :].split(":")) for key in keys)

    def update_script(
        self, kind: str, name: str, version: str, script: dict
    ) -> None:
        """
        Update processing script definition.

        :param kind: script kind
        :param name: script name
        :param version: script version
        :param script: script definition

        """
        self._update(self._script_path(kind, name, version), script)

    def delete_script(self, kind: str, name: str, version: str) -> None:
        """
        Delete processing script definition.

        :param kind: script kind
        :param name: script name
        :param version: script version

        """
        path = self._script_path(kind, name, version)
        self._delete(path, recurse=True)

    # -------------------------------------
    # Private methods
    # -------------------------------------

    def _script_path(self, kind: str, name: str, version: str) -> str:
        """
        Construct processing script definition path.

        :param kind: script kind
        :param name: script name
        :param version: script version

        :returns: path
        """
        script_path = self._paths["script"]
        return f"{script_path}{kind}:{name}:{version}"
