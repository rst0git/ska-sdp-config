"""Configuration entities."""

from .deployment import Deployment
from .pb import ProcessingBlock

__all__ = [
    "Deployment",
    "ProcessingBlock",
]
