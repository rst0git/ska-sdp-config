"""
Command line utility for interacting with SKA Science Data Processor (SDP).

Usage:
    ska-sdp COMMAND [options] [SDP_OBJECT] [<args>...]
    ska-sdp COMMAND (-h|--help)
    ska-sdp (-h|--help)

SDP Objects:
    pb           Interact with processing blocks
    script       Interact with available processing script definitions
    deployment   Interact with deployments
    eb           Interact with execution blocks
    controller   Interact with Tango controller device
    subarray     Interact with Tango subarray device

Commands:
    list           List information of object from the Configuration DB
    get | watch    Print all the information (i.e. value) of a key in the Config DB
    create         Create a new, raw key-value pair in the Config DB;
                   Run a processing script; Create a deployment
    update         Update a raw key value from CLI
    edit           Edit a raw key value from text editor
    delete         Delete a single key or all keys within a path from the Config DB
    end            Stop/Cancel execution block
    import         Import processing script definitions from file or URL
"""  # noqa: E501

import logging
import sys

from docopt import docopt

from ska_sdp_config import config
from ska_sdp_config.ska_sdp_cli import (
    sdp_create,
    sdp_delete,
    sdp_end,
    sdp_get,
    sdp_import,
    sdp_list,
    sdp_update,
)

LOG = logging.getLogger("ska-sdp")
LOG.setLevel(logging.INFO)
LOG.addHandler(logging.StreamHandler(sys.stdout))

COMMAND = "COMMAND"


def main(argv=None):
    """Run ska-sdp."""
    if argv is None:
        argv = sys.argv[1:]

    args = docopt(__doc__, argv=argv, options_first=True)
    cfg = config.Config()

    if args[COMMAND] == "list":
        sdp_list.main(argv, cfg)

    elif args[COMMAND] == "get" or args[COMMAND] == "watch":
        sdp_get.main(argv, cfg)

    elif args[COMMAND] == "create":
        sdp_create.main(argv, cfg)

    elif args[COMMAND] == "update" or args[COMMAND] == "edit":
        sdp_update.main(argv, cfg)

    elif args[COMMAND] == "delete":
        sdp_delete.main(argv, cfg)

    elif args[COMMAND] == "import":
        sdp_import.main(argv, cfg)

    elif args[COMMAND] == "end":
        sdp_end.main(argv, cfg)

    else:
        LOG.error(
            "Command '%s' is not supported. Run 'ska-sdp --help' to view "
            "usage.",
            args[COMMAND],
        )


if __name__ == "__main__":
    main()
