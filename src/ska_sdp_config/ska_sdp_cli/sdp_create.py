"""
Create SDP objects (deployment, script, eb) in the Configuration Database.
Create a processing block to run a script.

Usage:
    ska-sdp create [options] pb <script> [<parameters>] [--eb=<eb-parameters>]
    ska-sdp create [options] deployment <item-id> <kind> <parameters>
    ska-sdp create [options] (script|eb) <item-id> <value>
    ska-sdp create (-h|--help)

Arguments:
    <script>            Script that the processing block will run, in the format:
                            kind:name:version
    <parameters>        Optional parameters for a script, with expected format:
                            '{"key1": "value1", "key2": "value2"}'
                        For deployments, expected format:
                            '{"chart": <chart-name>, "values": <dict-of-values>}'
    <eb-parameters>     Optional eb parameters for a real-time script
    <item-id>           Id of the new deployment, script or eb
    <kind>              Kind of the new deployment (currently "helm" only)

Options:
    -h, --help     Show this screen
    -q, --quiet    Cut back on unnecessary output

Example:
    ska-sdp create eb eb-test-20210524-00000 '{"test": true}'
    Result in the config db:
        key: /eb/eb-test-20210524-00000
        value: {"test": true}

Note: You cannot create processing blocks apart from when they are called to run a script.
"""  # noqa: E501

import logging

import yaml
from docopt import docopt

from ska_sdp_config import entity

LOG = logging.getLogger("ska-sdp")


def cmd_create(txn, key, value):
    """
    Create raw key.

    :param txn: Config object transaction
    :param key: key within the config db to be created
    :param value: value of new key to be added
    """
    txn.raw.create(key, value)


def cmd_create_eb(txn, pb_id, parameters):
    """
    Create an EB for a real-time processing block.

    :param txn: Config object transaction
    :param pb_id: processing block ID
    :param parameters: execution block parameters, it can be None

    :return eb_id : ID of the created execution blocks
    """
    # Create new execution block ID
    eb_id = txn.new_execution_block_id("sdpcli")

    # Initial EB parameters
    eblock = {
        "eb_id": eb_id,
        "subarray_id": None,
        "scan_types": [],
        "pb_realtime": [pb_id],
        "pb_batch": [],
        "pb_receive_addresses": None,
        "current_scan_type": None,
        "scan_id": None,
        "status": "ACTIVE",
    }

    # Update EB with parameters from command line
    if parameters is not None:
        pars = yaml.safe_load(parameters)
        eblock.update(pars)

    # Create the EB
    txn.create_execution_block(eb_id, eblock)

    return eb_id


def cmd_create_pb(txn, script, parameters, eb_pars):
    """
    Create a processing block to run a script.

    :param txn: Config object transaction
    :param script: dict of script information: kind, name, version
    :param parameters: dict of script parameters, it can be None
    :param eb_pars: dict of execution block parameters, it can be
                     None

    :return pb_id: ID of the created processing block
    """
    # Initialise variables
    eb_id = None

    # Parse parameters
    if parameters is not None:
        pars = yaml.safe_load(parameters)
    else:
        pars = {}

    # Create new processing block ID, create processing block
    pb_id = txn.new_processing_block_id("sdpcli")

    if script.get("kind") == "realtime":
        eb_id = cmd_create_eb(txn, pb_id, eb_pars)
        LOG.info("Execution block created with eb_id: %s", eb_id)
        LOG.info("The EB can be ended by running: ska-sdp end eb %s", eb_id)

    txn.create_processing_block(
        entity.ProcessingBlock(pb_id, eb_id, script, parameters=pars)
    )

    return pb_id


def cmd_deploy(txn, deploy_kind, deploy_id, parameters):
    """
    Create a deployment.

    :param txn: Config object transaction
    :param deploy_kind: Kind of deployment, currently "helm" only
    :param deploy_id: ID of the deployment
    :param parameters: String of deployment parameters in the form of:
                       '{"chart": <chart-name>, "values": <dict-of-values>}'
    """
    params_dict = yaml.safe_load(parameters)
    txn.create_deployment(
        entity.Deployment(deploy_id, deploy_kind, params_dict)
    )


def main(argv, config):
    """Run ska-sdp create."""
    args = docopt(__doc__, argv=argv)

    object_dict = {"script": args["script"], "eb": args["eb"]}

    if args["pb"]:
        script = args["<script>"].split(":")
        if len(script) != 3:
            raise ValueError("Please specify script as 'kind:name:version'!")

        script = {
            "kind": script[0],
            "name": script[1],
            "version": script[2],
        }

        for txn in config.txn():
            pb_id = cmd_create_pb(
                txn, script, args["<parameters>"], args["--eb"]
            )

        LOG.info("Processing block created with pb_id: %s", pb_id)
        return

    if args["deployment"]:
        for txn in config.txn():
            cmd_deploy(
                txn, args["<kind>"], args["<item-id>"], args["<parameters>"]
            )

        LOG.info("Deployment created with id: %s", args["<item-id>"])
        return

    path = "/"
    for sdp_object, exists in object_dict.items():
        if exists:
            path = path + sdp_object
            break  # only one can be true, or none

    path = path + "/" + args["<item-id>"]

    for txn in config.txn():
        cmd_create(txn, path, args["<value>"])

    LOG.info("%s created", path)
