"""
Import processing script definitions into the Configuration Database.

Usage:
    ska-sdp import scripts [options] <file-or-url>
    ska-sdp import (-h|--help)

Arguments:
    <file-or-url>      File or URL to import script definitions from.

Options:
    -h, --help          Show this screen
    --sync              Delete scripts not in the input
"""

import logging
import os
from typing import Dict

import requests
import yaml
from docopt import docopt
from yaml.parser import ParserError
from yaml.scanner import ScannerError

from ska_sdp_config.config import Transaction

LOG = logging.getLogger("ska-sdp")


def read_input(input_object: str) -> Dict:
    """
    Read script definitions from file or URL.

    :param input_object: input filename or URL
    :returns: definitions converted into dict
    """
    if os.path.isfile(input_object):
        with open(input_object, "r", encoding="utf8") as file:
            data = file.read()
    else:
        with requests.get(input_object, timeout=60.0) as response:
            data = response.text

    definitions = yaml.safe_load(data)

    return definitions


def _parse_structured(definitions: Dict) -> Dict:
    """
    Parse structured script definitions.

    :param definitions: structured script definitions
    :returns: dictionary mapping (kind, name, version) to definition.

    """
    repositories = {
        repo["name"]: repo["path"] for repo in definitions["repositories"]
    }
    scripts = {
        (w["kind"], w["name"], v): {
            "image": repositories[w["repository"]]
            + "/"
            + w["image"]
            + ":"
            + v,
        }
        for w in definitions["scripts"]
        for v in w["versions"]
    }
    return scripts


def _parse_flat(definitions: Dict) -> Dict:
    """
    Parse flat script definitions.

    :param definitions: flat script definitions
    :returns: dictionary mapping (kind, name, version) to definition.

    """
    scripts = {
        (w["kind"], w["name"], w["version"]): {"image": w["image"]}
        for w in definitions["scripts"]
    }
    return scripts


def parse_definitions(definitions: Dict) -> Dict:
    """
    Parse script definitions.

    :param definitions: script definitions
    :returns: dictionary mapping (kind, name, version) to definition.

    """
    if "repositories" in definitions:
        scripts = _parse_structured(definitions)
    else:
        scripts = _parse_flat(definitions)
    return scripts


def import_scripts(txn: Transaction, scripts: Dict, sync: bool = True):
    """
    Import the script definitions into the Configuration Database.

    :param txn: Config object transaction
    :param scripts: script definitions
    :param sync: delete scripts not in the input
    """
    # Create sorted list of existing and new scripts
    existing_scripts = txn.list_scripts()
    all_scripts = sorted(list(set(existing_scripts) | set(scripts.keys())))

    change = False
    for key in all_scripts:
        if key in scripts:
            old_value = txn.get_script(*key)
            new_value = scripts[key]
            if old_value is None:
                LOG.info("Creating %s", key)
                txn.create_script(*key, new_value)
                change = True
            elif new_value != old_value:
                LOG.info("Updating %s", key)
                txn.update_script(*key, new_value)
                change = True
        elif sync:
            LOG.info("Deleting %s", key)
            txn.delete_script(*key)
            change = True
    if not change:
        LOG.info("No changes")


def main(argv, config):
    """Run ska-sdp import."""
    args = docopt(__doc__, argv=argv)

    LOG.info(
        "Importing processing script definitions from %s",
        args["<file-or-url>"],
    )

    try:
        definitions = read_input(args["<file-or-url>"])
    except (
        requests.exceptions.MissingSchema,
        requests.exceptions.ConnectionError,
    ):
        # MissingSchema exception raised when json file doesn't exist
        # ConnectionError raised when URL is wrong
        LOG.error("Bad file name or URL. Please fix, and retry.")
        return
    except (ParserError, ScannerError):
        LOG.error("Malformed YAML/JSON file. Please fix, and retry.")
        return

    scripts = parse_definitions(definitions)

    for txn in config.txn():
        import_scripts(txn, scripts, sync=args["--sync"])

    LOG.info("Import finished successfully.")
